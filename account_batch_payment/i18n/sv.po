# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * account_batch_deposit
# 
# Translators:
# Martin Trigaux, 2018
# Kristoffer Grundström <kristoffer.grundstrom1983@gmail.com>, 2018
# Anders Wallenquist <anders.wallenquist@vertel.se>, 2018
# Martin Wilderoth <martin.wilderoth@linserv.se>, 2018
# Patrik Lermon <patrik.lermon@gmail.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~11.3+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-25 08:34+0000\n"
"PO-Revision-Date: 2018-06-25 08:34+0000\n"
"Last-Translator: Patrik Lermon <patrik.lermon@gmail.com>, 2018\n"
"Language-Team: Swedish (https://www.transifex.com/odoo/teams/41243/sv/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: sv\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: account_batch_deposit
#: code:addons/account_batch_deposit/models/account_payment.py:42
#, python-format
msgid ""
"All payments to print as a deposit slip must belong to the same journal."
msgstr ""

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit__amount
#: model:ir.ui.view,arch_db:account_batch_deposit.print_batch_deposit
msgid "Amount"
msgstr "Belopp"

#. module: account_batch_deposit
#: model:ir.model.fields,help:account_batch_deposit.field_account_journal__batch_deposit_sequence_id
msgid "Automatically generates references for batch deposits."
msgstr ""

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit__journal_id
msgid "Bank"
msgstr "Bank"

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_search
msgid "Bank Journal"
msgstr "Bankjournal"

#. module: account_batch_deposit
#: model:account.payment.method,name:account_batch_deposit.account_payment_method_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_payment__batch_deposit_id
#: model:ir.ui.view,arch_db:account_batch_deposit.account_journal_dashboard_kanban_view_inherited
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_search
msgid "Batch Deposit"
msgstr ""

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_journal__batch_deposit_payment_method_selected
msgid "Batch Deposit Payment Method Selected"
msgstr ""

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_journal__batch_deposit_sequence_id
msgid "Batch Deposit Sequence"
msgstr ""

#. module: account_batch_deposit
#: model:ir.actions.act_window,name:account_batch_deposit.action_batch_deposit
#: model:ir.ui.menu,name:account_batch_deposit.menu_batch_deposit
msgid "Batch Deposits"
msgstr ""

#. module: account_batch_deposit
#: code:addons/account_batch_deposit/models/account_journal.py:44
#, python-format
msgid "Batch Deposits Sequence"
msgstr ""

#. module: account_batch_deposit
#: model:ir.actions.act_window,help:account_batch_deposit.action_batch_deposit
msgid ""
"Batch deposits allows you to group received checks before you deposit them to the bank.\n"
"                    The amount deposited to your bank will then appear as a single transaction on your bank statement.\n"
"                    When you proceed with the reconciliation, simply select the corresponding batch deposit to reconcile the payments."
msgstr ""

#. module: account_batch_deposit
#: code:addons/account_batch_deposit/models/account_journal.py:69
#: model:ir.actions.server,name:account_batch_deposit.action_account_create_batch_deposit
#, python-format
msgid "Create Batch Deposit"
msgstr ""

#. module: account_batch_deposit
#: model:ir.actions.act_window,help:account_batch_deposit.action_batch_deposit
msgid "Create a new batch deposit"
msgstr ""

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit__create_uid
msgid "Created by"
msgstr "Skapad av"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit__create_date
msgid "Created on"
msgstr "Skapad den"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit__currency_id
msgid "Currency"
msgstr "Valuta"

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.print_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "Customer"
msgstr "Kund"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit__date
#: model:ir.ui.view,arch_db:account_batch_deposit.print_batch_deposit
msgid "Date"
msgstr "Datum"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit__display_name
#: model:ir.model.fields,field_description:account_batch_deposit.field_report_account_batch_deposit_print_batch_deposit__display_name
msgid "Display Name"
msgstr "Visningsnamn"

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_search
msgid "Group By"
msgstr "Gruppera på"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit__id
#: model:ir.model.fields,field_description:account_batch_deposit.field_report_account_batch_deposit_print_batch_deposit__id
msgid "ID"
msgstr "ID"

#. module: account_batch_deposit
#. openerp-web
#: code:addons/account_batch_deposit/static/src/js/account_batch_deposit_reconciliation.js:135
#, python-format
msgid "Incorrect Operation"
msgstr "Felaktig operation"

#. module: account_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_account_journal
msgid "Journal"
msgstr "Journal"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit____last_update
#: model:ir.model.fields,field_description:account_batch_deposit.field_report_account_batch_deposit_print_batch_deposit____last_update
msgid "Last Modified on"
msgstr "Senast redigerad"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit__write_uid
msgid "Last Updated by"
msgstr "Senast uppdaterad av"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit__write_date
msgid "Last Updated on"
msgstr "Senast uppdaterad"

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.print_batch_deposit
msgid "Memo"
msgstr "Memo"

#. module: account_batch_deposit
#: selection:account.batch.deposit,state:0
msgid "New"
msgstr "Ny"

#. module: account_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_account_payment
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit__payment_ids
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "Payments"
msgstr "Betalningar"

#. module: account_batch_deposit
#: code:addons/account_batch_deposit/models/account_payment.py:38
#, python-format
msgid ""
"Payments to print as a deposit slip must have 'Batch Deposit' selected as "
"payment method, not be part of an existing batch deposit and not have "
"already been reconciled"
msgstr ""

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "Print"
msgstr "Skriv ut"

#. module: account_batch_deposit
#: model:ir.actions.report,name:account_batch_deposit.action_print_batch_deposit
msgid "Print Batch Deposit"
msgstr ""

#. module: account_batch_deposit
#: model:ir.actions.server,name:account_batch_deposit.action_account_print_batch_deposit
msgid "Print Batch Deposits"
msgstr ""

#. module: account_batch_deposit
#: selection:account.batch.deposit,state:0
msgid "Printed"
msgstr "Printed"

#. module: account_batch_deposit
#: selection:account.batch.deposit,state:0
msgid "Reconciled"
msgstr "Avstämd"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit__name
msgid "Reference"
msgstr "Referens"

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "Register Payment"
msgstr "Registrera betalning"

#. module: account_batch_deposit
#. openerp-web
#: code:addons/account_batch_deposit/static/src/xml/account_reconciliation.xml:8
#, python-format
msgid "Select a Batch Deposit"
msgstr ""

#. module: account_batch_deposit
#. openerp-web
#: code:addons/account_batch_deposit/static/src/js/account_batch_deposit_reconciliation.js:133
#, python-format
msgid ""
"Some journal items from the selected batch deposit are already selected in "
"another reconciliation : "
msgstr ""

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit__state
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_search
msgid "State"
msgstr "Status"

#. module: account_batch_deposit
#: model:ir.model.fields,help:account_batch_deposit.field_account_journal__batch_deposit_payment_method_selected
msgid ""
"Technical feature used to know whether batch deposit was enabled as payment "
"method."
msgstr ""

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "Total"
msgstr "Totalt"

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_search
msgid "Unreconciled"
msgstr "Oavstämd"

#. module: account_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_account_reconciliation_widget
msgid "account.reconciliation.widget"
msgstr ""

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "auto ..."
msgstr ""

#. module: account_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_report_account_batch_deposit_print_batch_deposit
msgid "report.account_batch_deposit.print_batch_deposit"
msgstr ""
