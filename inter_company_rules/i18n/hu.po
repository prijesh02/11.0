# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * inter_company_rules
# 
# Translators:
# Martin Trigaux, 2016
# krnkris, 2016
# gezza <geza.nagy@oregional.hu>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~11.3+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-25 08:34+0000\n"
"PO-Revision-Date: 2018-06-25 08:34+0000\n"
"Last-Translator: gezza <geza.nagy@oregional.hu>, 2016\n"
"Language-Team: Hungarian (https://www.transifex.com/odoo/teams/41243/hu/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: hu\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: inter_company_rules
#: code:addons/inter_company_rules/models/account_invoice.py:89
#, python-format
msgid " Invoice: "
msgstr "Számla:"

#. module: inter_company_rules
#: model:ir.ui.view,arch_db:inter_company_rules.res_config_settings_view_form
msgid ", generate a"
msgstr ""

#. module: inter_company_rules
#: model:ir.ui.view,arch_db:inter_company_rules.view_company_inter_change_inherit_form
msgid ""
"<strong>\n"
"                                    <span attrs=\"{'invisible': [('applicable_on','!=', 'purchase')]}\" class=\"ml4\">Sale Order</span>\n"
"                                    <span attrs=\"{'invisible': [('applicable_on','!=', 'sale')]}\" class=\"ml4\">Purchase Order</span>\n"
"                                    <span attrs=\"{'invisible':[('applicable_on','!=', 'sale_purchase')]}\" class=\"ml4\"> Sale and Purchase Order</span>\n"
"                                </strong> using"
msgstr ""

#. module: inter_company_rules
#: model:ir.ui.view,arch_db:inter_company_rules.res_config_settings_view_form
msgid ""
"<strong>\n"
"                                <span attrs=\"{'invisible': [('applicable_on','!=', 'purchase')]}\" class=\"ml8\">Sale Order </span>\n"
"                                <span attrs=\"{'invisible': [('applicable_on','!=', 'sale')]}\" class=\"ml8\">Purchase Order </span>\n"
"                                <span attrs=\"{'invisible': [('applicable_on','!=', 'sale_purchase')]}\" class=\"ml8\"> Sale and Purchase Order </span>\n"
"                            </strong>\n"
"                            using"
msgstr ""

#. module: inter_company_rules
#: model:ir.model.fields,field_description:inter_company_rules.field_res_company__applicable_on
#: model:ir.model.fields,field_description:inter_company_rules.field_res_config_settings__applicable_on
msgid "Applicable On"
msgstr "Ezen alkalmazandó"

#. module: inter_company_rules
#: model:ir.model.fields,field_description:inter_company_rules.field_account_invoice__auto_generated
msgid "Auto Generated Document"
msgstr "Automatikusan létrehozott dokumentum"

#. module: inter_company_rules
#: model:ir.model.fields,field_description:inter_company_rules.field_purchase_order__auto_generated
msgid "Auto Generated Purchase Order"
msgstr "Automatikusan létrehozott beszerzési megrendelés"

#. module: inter_company_rules
#: model:ir.model.fields,field_description:inter_company_rules.field_sale_order__auto_generated
msgid "Auto Generated Sales Order"
msgstr ""

#. module: inter_company_rules
#: model:ir.model.fields,field_description:inter_company_rules.field_res_company__auto_validation
#: model:ir.model.fields,field_description:inter_company_rules.field_res_config_settings__auto_validation
msgid "Auto Validation"
msgstr ""

#. module: inter_company_rules
#: model:ir.model,name:inter_company_rules.model_res_company
msgid "Companies"
msgstr "Vállalatok"

#. module: inter_company_rules
#: code:addons/inter_company_rules/models/purchase_order.py:82
#: code:addons/inter_company_rules/models/sale_order.py:77
#, python-format
msgid ""
"Configure correct warehouse for company(%s) from Menu: "
"Settings/Users/Companies"
msgstr ""
"Állítson be megfelelő raktárépületet a (%s) vállalathoz a következő Menüből:"
" Beállítások/Felhasználók/Vállalatok"

#. module: inter_company_rules
#: model:ir.model.fields,help:inter_company_rules.field_res_config_settings__warehouse_id
msgid ""
"Default value to set on Purchase Orders that will be created based on Sales "
"Orders made to this company."
msgstr ""

#. module: inter_company_rules
#: model:ir.model.fields,help:inter_company_rules.field_res_company__warehouse_id
msgid ""
"Default value to set on Purchase(Sales) Orders that will be created based on"
" Sale(Purchase) Orders made to this company"
msgstr ""
"A Beszerzési(Vásárlói/Értékesítési) Megrendeléseken beállított "
"alapértelmezett érték, melyet a vállalat a Vásárlói(Beszerzési) megrendelés "
"alapján hozza létre"

#. module: inter_company_rules
#: model:ir.model.fields,field_description:inter_company_rules.field_res_company__intercompany_user_id
msgid "Inter Company User"
msgstr "Belső vállalati felhasználó"

#. module: inter_company_rules
#: code:addons/inter_company_rules/models/purchase_order.py:42
#: code:addons/inter_company_rules/models/sale_order.py:46
#, python-format
msgid "Inter company user of company %s doesn't have enough access rights"
msgstr ""
"Belső vállalkozás %s vállalat felhasználójának nincs megfelelő hozzáférési "
"jogosultsága"

#. module: inter_company_rules
#: model:ir.ui.view,arch_db:inter_company_rules.view_company_inter_change_inherit_form
msgid "Inter-Company Rules"
msgstr "Belső vállalati szabályok"

#. module: inter_company_rules
#: model:ir.model,name:inter_company_rules.model_account_invoice
msgid "Invoice"
msgstr "Számla"

#. module: inter_company_rules
#: code:addons/inter_company_rules/models/account_invoice.py:81
#, python-format
msgid "Please define %s journal for this company: \"%s\" (id:%d)."
msgstr "Kérem határozzon meg %s naplót ehhez a vállalthoz: \"%s\" (id:%d)."

#. module: inter_company_rules
#: code:addons/inter_company_rules/models/purchase_order.py:39
#, python-format
msgid "Provide at least one user for inter company relation for % "
msgstr ""
"Szolgáltasson legalább egy felhasználót a belső vállalathoz ami "
"hozzátartozik ehhez % "

#. module: inter_company_rules
#: code:addons/inter_company_rules/models/account_invoice.py:44
#: code:addons/inter_company_rules/models/sale_order.py:43
#, python-format
msgid "Provide one user for intercompany relation for % "
msgstr ""
"Szolgáltasson egy felhasználót a belső vállalathoz ami hozzátartozik ehhez %"
" "

#. module: inter_company_rules
#: model:ir.model,name:inter_company_rules.model_purchase_order
#: selection:res.company,applicable_on:0
#: selection:res.config.settings,applicable_on:0
msgid "Purchase Order"
msgstr "Beszerzési megrendelés"

#. module: inter_company_rules
#: model:ir.model,name:inter_company_rules.model_sale_order
msgid "Quotation"
msgstr "Árajánlat"

#. module: inter_company_rules
#: model:ir.model.fields,help:inter_company_rules.field_res_company__intercompany_user_id
msgid ""
"Responsible user for creation of documents triggered by intercompany rules."
msgstr ""
"Felelős felhasználó a belső vállalati szabályok által létrehozott "
"dokumentumokhoz."

#. module: inter_company_rules
#: model:ir.model.fields,field_description:inter_company_rules.field_res_company__rule_type
#: model:ir.model.fields,field_description:inter_company_rules.field_res_config_settings__rule_type
msgid "Rule Type"
msgstr ""

#. module: inter_company_rules
#: selection:res.company,applicable_on:0
#: selection:res.config.settings,applicable_on:0
msgid "Sale Order"
msgstr "Vásárlói megrendelés"

#. module: inter_company_rules
#: selection:res.company,applicable_on:0
#: selection:res.config.settings,applicable_on:0
msgid "Sale and Purchase Order"
msgstr ""

#. module: inter_company_rules
#: model:ir.model.fields,field_description:inter_company_rules.field_res_config_settings__rules_company_id
msgid "Select Company"
msgstr "Vállalat kiválasztása"

#. module: inter_company_rules
#: model:ir.model.fields,help:inter_company_rules.field_res_config_settings__rules_company_id
msgid "Select company to setup Inter company rules."
msgstr "Belső vállalati szabályok beállításához válasszon vállalkozást."

#. module: inter_company_rules
#: model:ir.model.fields,help:inter_company_rules.field_res_company__rule_type
#: model:ir.model.fields,help:inter_company_rules.field_res_config_settings__rule_type
msgid "Select the type to setup inter company rules in selected company."
msgstr ""
"A kiválasztott vállalathoz tartozó belső vállalati szabályok beállításához "
"válasszon típust."

#. module: inter_company_rules
#: model:ir.model.fields,field_description:inter_company_rules.field_account_invoice__auto_invoice_id
msgid "Source Invoice"
msgstr "Forrás számla"

#. module: inter_company_rules
#: model:ir.model.fields,field_description:inter_company_rules.field_sale_order__auto_purchase_order_id
msgid "Source Purchase Order"
msgstr "Forrás Beszerzési megrendelés"

#. module: inter_company_rules
#: model:ir.model.fields,field_description:inter_company_rules.field_purchase_order__auto_sale_order_id
msgid "Source Sales Order"
msgstr ""

#. module: inter_company_rules
#: selection:res.company,rule_type:0 selection:res.config.settings,rule_type:0
msgid "Synchronize Invoices & Bills"
msgstr ""

#. module: inter_company_rules
#: selection:res.company,rule_type:0 selection:res.config.settings,rule_type:0
msgid "Synchronize Sales & Purchase Orders"
msgstr ""

#. module: inter_company_rules
#: model:ir.model.fields,field_description:inter_company_rules.field_res_company__warehouse_id
msgid "Warehouse"
msgstr "Raktár"

#. module: inter_company_rules
#: model:ir.model.fields,field_description:inter_company_rules.field_res_config_settings__warehouse_id
msgid "Warehouse For Purchase Orders"
msgstr "Raktár a beszerzési megrendelésekhez"

#. module: inter_company_rules
#: model:ir.ui.view,arch_db:inter_company_rules.res_config_settings_view_form
#: model:ir.ui.view,arch_db:inter_company_rules.view_company_inter_change_inherit_form
msgid "When a company validates a"
msgstr ""

#. module: inter_company_rules
#: code:addons/inter_company_rules/models/purchase_order.py:47
#, python-format
msgid ""
"You cannot create SO from PO because sale price list currency is different "
"than purchase price list currency."
msgstr ""
"Nem hozhat létre Vásárlói Megrendelést /VM/ a Beszerzési Megrendelésből "
"/BM/, mert az értékesítési árlista pénzneme eltér a beszerzési árlista "
"pénznemétől."

#. module: inter_company_rules
#: code:addons/inter_company_rules/models/res_company.py:37
#, python-format
msgid ""
"You cannot select to create invoices based on other invoices\n"
"                    simultaneously with another option ('Create Sales Orders when buying to this\n"
"                    company' or 'Create Purchase Orders when selling to this company')!"
msgstr ""

#. module: inter_company_rules
#: selection:res.company,auto_validation:0
#: selection:res.config.settings,auto_validation:0
msgid "draft"
msgstr ""

#. module: inter_company_rules
#: model:ir.ui.view,arch_db:inter_company_rules.view_company_inter_change_inherit_form
msgid "generate a"
msgstr ""

#. module: inter_company_rules
#: model:ir.model,name:inter_company_rules.model_res_config_settings
msgid "res.config.settings"
msgstr "res.config.settings"

#. module: inter_company_rules
#: model:ir.ui.view,arch_db:inter_company_rules.res_config_settings_view_form
msgid "to"
msgstr "ig"

#. module: inter_company_rules
#: model:ir.ui.view,arch_db:inter_company_rules.view_company_inter_change_inherit_form
msgid "to this company,"
msgstr ""

#. module: inter_company_rules
#: selection:res.company,auto_validation:0
#: selection:res.config.settings,auto_validation:0
msgid "validated"
msgstr ""
