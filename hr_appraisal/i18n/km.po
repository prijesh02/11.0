# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * hr_appraisal
# 
# Translators:
# Sengtha Chay <sengtha@gmail.com>, 2018
# Chan Nath <channath@gmail.com>, 2018
# Samkhann Seang <seangsamkhann@gmail.com>, 2018
# AN Souphorn <ansouphorn@gmail.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~11.5+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-02 10:31+0000\n"
"PO-Revision-Date: 2018-10-02 10:31+0000\n"
"Last-Translator: AN Souphorn <ansouphorn@gmail.com>, 2018\n"
"Language-Team: Khmer (https://www.transifex.com/odoo/teams/41243/km/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: km\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_kanban
msgid ") Answers"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_kanban
msgid ") Sent Appraisal"
msgstr ""

#. module: hr_appraisal
#: code:addons/hr_appraisal/models/hr_appraisal.py:127
#, python-format
msgid ""
"<a href=\"#\" data-oe-model=\"%s\" data-oe-id=\"%s\">Meeting</a> for <a "
"href=\"#\" data-oe-model=\"%s\" data-oe-id=\"%s\">%s's</a> appraisal"
msgstr ""

#. module: hr_appraisal
#: model:mail.template,body_html:hr_appraisal.send_appraisal_template
msgid ""
"<div style=\"margin: 0px; padding: 0px; font-size: 13px;\">\n"
"    <p style=\"margin: 0px; padding: 0px; font-size: 13px;\">\n"
"        Hello ${ctx.get('employee').name}<br/><br/>\n"
"        % if ctx.get('employee').id == object.employee_id.id:\n"
"            Please fill out the following survey related to your appraisal.\n"
"        % else:\n"
"            Please fill out the following survey related to ${object.employee_id.name}'s appraisal.\n"
"        % endif\n"
"        <div style=\"margin: 16px 0px 16px 0px;\">\n"
"            <a href=\"__URL__\" style=\"background-color: #875A7B; padding: 8px 16px 8px 16px; text-decoration: none; color: #fff; border-radius: 5px; font-size:13px;\">\n"
"                Start Survey\n"
"            </a>\n"
"        </div>\n"
"        Please answer the appraisal for ${format_date(object.date_close)}.<br/>\n"
"        Thank you for your participation.\n"
"    </p>\n"
"</div>\n"
"            "
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_kanban
msgid "<strong><span>Final Interview: </span></strong>"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__message_needaction
msgid "Action Needed"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__action_plan
msgid "Action Plan"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__active
msgid "Active"
msgstr "សកម្ម"

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__activity_ids
msgid "Activities"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__activity_state
msgid "Activity State"
msgstr ""

#. module: hr_appraisal
#: model:ir.actions.act_window,name:hr_appraisal.mail_activity_type_action_config_hr_appraisal
#: model:ir.ui.menu,name:hr_appraisal.hr_appraisal_menu_config_activity_type
msgid "Activity Types"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__survey_completed_ids
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_hr_appraisal_form
msgid "Answers"
msgstr ""

#. module: hr_appraisal
#: model:ir.actions.act_window,name:hr_appraisal.open_view_hr_appraisal_tree
#: model:ir.actions.act_window,name:hr_appraisal.open_view_hr_appraisal_tree2
#: model:ir.model.fields,field_description:hr_appraisal.field_survey_user_input__appraisal_id
#: model:ir.ui.menu,name:hr_appraisal.menu_hr_appraisal_root
#: model:ir.ui.menu,name:hr_appraisal.menu_open_view_hr_appraisal_tree
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_employee_view_form
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_hr_appraisal_form
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_hr_appraisal_tree
msgid "Appraisal"
msgstr ""

#. module: hr_appraisal
#: model:ir.actions.act_window,name:hr_appraisal.action_appraisal_report_all
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_appraisal_report_graph
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_appraisal_report_pivot
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_appraisal_report_search
msgid "Appraisal Analysis"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__appraisal_collaborators_ids
msgid "Appraisal Collaborators"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__appraisal_colleagues_ids
msgid "Appraisal Colleagues"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__date_close
msgid "Appraisal Deadline"
msgstr ""

#. module: hr_appraisal
#: model:mail.activity.type,name:hr_appraisal.mail_act_appraisal_form
msgid "Appraisal Form to Fill"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_employee_view_form
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_hr_appraisal_form
msgid "Appraisal Form..."
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__appraisal_frequency_unit
msgid "Appraisal Frequency"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__appraisal_manager_ids
msgid "Appraisal Manager"
msgstr ""

#. module: hr_appraisal
#: code:addons/hr_appraisal/models/hr_appraisal.py:118
#, python-format
msgid "Appraisal Meeting For %s"
msgstr ""

#. module: hr_appraisal
#: model:mail.template,subject:hr_appraisal.send_appraisal_template
msgid "Appraisal Regarding"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__appraisal_frequency
msgid "Appraisal Repeat Every"
msgstr ""

#. module: hr_appraisal
#: code:addons/hr_appraisal/models/hr_appraisal.py:239
#: selection:hr.appraisal,state:0 selection:hr.appraisal.report,state:0
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_appraisal_report_search
#, python-format
msgid "Appraisal Sent"
msgstr ""

#. module: hr_appraisal
#: model:ir.model,name:hr_appraisal.model_hr_appraisal_report
msgid "Appraisal Statistics"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__manager_appraisal
msgid "Appraisal by Manager"
msgstr ""

#. module: hr_appraisal
#: code:addons/hr_appraisal/models/hr_appraisal.py:186
#, python-format
msgid "Appraisal form(s) have been sent"
msgstr ""

#. module: hr_appraisal
#: model:ir.actions.act_window,name:hr_appraisal.hr_appraisal_action_from_department
msgid "Appraisal to start"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_department_view_kanban
msgid "Appraisal(s)"
msgstr ""

#. module: hr_appraisal
#: model:ir.actions.server,name:hr_appraisal.ir_cron_scheduler_appraisal_ir_actions_server
#: model:ir.cron,cron_name:hr_appraisal.ir_cron_scheduler_appraisal
#: model:ir.cron,name:hr_appraisal.ir_cron_scheduler_appraisal
msgid "Appraisal: run employee appraisal"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__appraisal_count
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_department_view_kanban
msgid "Appraisals"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_department__appraisals_to_process_count
msgid "Appraisals to Process"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_search
msgid "Archived"
msgstr "ឯកសារ"

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__message_attachment_count
msgid "Attachment Count"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_hr_appraisal_form
msgid "Cancel"
msgstr "លុបចោល"

#. module: hr_appraisal
#: code:addons/hr_appraisal/models/hr_appraisal.py:239
#: selection:hr.appraisal,state:0 selection:hr.appraisal.report,state:0
#, python-format
msgid "Cancelled"
msgstr "បានលុបចោល"

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__collaborators_appraisal
msgid "Collaborator"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__collaborators_survey_id
msgid "Collaborator's Appraisal"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__collaborators_ids
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__appraisal_by_collaborators
msgid "Collaborators"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__colleagues_survey_id
msgid "Colleague's Appraisal"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__colleagues_ids
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__appraisal_by_colleagues
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_hr_appraisal_form
msgid "Colleagues"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__colleagues_appraisal
msgid "Colleagues Appraisal"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__color
msgid "Color Index"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__company_id
msgid "Company"
msgstr "ក្រុមហ៊ុន"

#. module: hr_appraisal
#: model:ir.ui.menu,name:hr_appraisal.menu_hr_appraisal_configuration
msgid "Configuration"
msgstr "កំណត់ផ្លាស់ប្តូរ"

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal_report__create_date
msgid "Create Date"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.actions.act_window,help:hr_appraisal.open_view_hr_appraisal_tree
msgid "Create a new appraisal"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__create_uid
msgid "Created by"
msgstr "បង្កើតដោយ"

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__create_date
msgid "Created on"
msgstr "បង្កើតនៅ"

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_appraisal_report_search
msgid "Creation Date"
msgstr "ថ្ងៃបង្កើត"

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_appraisal_report_search
msgid "Date"
msgstr "កាលបរិច្ឆេត"

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal_report__deadline
msgid "Deadline"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_kanban
msgid "Deadline:"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_kanban
msgid "Delete"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__department_id
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal_report__department_id
msgid "Department"
msgstr "ដេប៉ាតឺម៉ង់"

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__display_name
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal_report__display_name
msgid "Display Name"
msgstr "ឈ្មោះសំរាប់បង្ហាញ"

#. module: hr_appraisal
#: code:addons/hr_appraisal/models/hr_appraisal.py:239
#: selection:hr.appraisal,state:0 selection:hr.appraisal.report,state:0
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_appraisal_report_search
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_hr_appraisal_form
#, python-format
msgid "Done"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_kanban
msgid "Dropdown menu"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_kanban
msgid "Edit"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__mail_template_id
msgid "Email Template for Appraisal"
msgstr ""

#. module: hr_appraisal
#: model:ir.model,name:hr_appraisal.model_hr_employee
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__employee_id
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal_report__employee_id
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__appraisal_self
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_search
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_appraisal_report_search
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_hr_appraisal_form
msgid "Employee"
msgstr "បុគ្គលិក"

#. module: hr_appraisal
#: model:ir.model,name:hr_appraisal.model_hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__employee_appraisal
msgid "Employee Appraisal"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__appraisal_employee
msgid "Employee Name"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__appraisal_colleagues_survey_id
msgid "Employee's Appraisal"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_hr_appraisal_form
msgid "Employee's Name"
msgstr ""

#. module: hr_appraisal
#: model:ir.model,name:hr_appraisal.model_calendar_event
msgid "Event"
msgstr "ព្រិត្តិការណ៍"

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_appraisal_report_search
msgid "Extended Filters..."
msgstr ""

#. module: hr_appraisal
#: code:addons/hr_appraisal/models/hr_appraisal.py:182
#, python-format
msgid ""
"Fill form <a href=\"%s\">%s</a> for <a href=\"#\" data-oe-model=\"%s\" data-"
"oe-id=\"%s\">%s's</a> appraisal"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_hr_appraisal_form
msgid "Final Evaluation"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__date_final_interview
msgid "Final Interview"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_appraisal_report_search
msgid "Final Interview Date"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__message_follower_ids
msgid "Followers"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__message_channel_ids
msgid "Followers (Channels)"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__message_partner_ids
msgid "Followers (Partners)"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_search
msgid "Future Activities"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_appraisal_report_search
msgid "Group By"
msgstr "ជា​ក្រុម​តាម"

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_search
msgid "Group by..."
msgstr ""

#. module: hr_appraisal
#: model:ir.model,name:hr_appraisal.model_hr_department
msgid "HR Department"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__id
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal_report__id
msgid "ID"
msgstr "ID"

#. module: hr_appraisal
#: model:ir.model.fields,help:hr_appraisal.field_hr_appraisal__message_unread
msgid "If checked new messages require your attention."
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,help:hr_appraisal.field_hr_appraisal__message_needaction
msgid "If checked, new messages require your attention."
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,help:hr_appraisal.field_hr_appraisal__message_has_error
msgid "If checked, some messages have a delivery error."
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,help:hr_appraisal.field_hr_appraisal__action_plan
msgid ""
"If the evaluation does not meet the expectations, you can propose an action "
"plan"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_kanban
msgid "In #{kanban_getcolorname(record.color.raw_value)}"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_appraisal_report_search
msgid "In progress Evaluations"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal_report__final_interview
msgid "Interview"
msgstr "សំភាសន៍"

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__message_is_follower
msgid "Is Follower"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal____last_update
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal_report____last_update
msgid "Last Modified on"
msgstr "កាលបរិច្ឆេតកែប្រែចុងក្រោយ"

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__write_uid
msgid "Last Updated by"
msgstr "ផ្លាស់ប្តូរចុងក្រោយ"

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__write_date
msgid "Last Updated on"
msgstr "ផ្លាស់ប្តូរចុងក្រោយ"

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_search
msgid "Late Activities"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__message_main_attachment_id
msgid "Main Attachment"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__manager_ids
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_hr_appraisal_form
#: model:res.groups,name:hr_appraisal.group_hr_appraisal_manager
msgid "Manager"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__manager_survey_id
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__appraisal_manager_survey_id
msgid "Manager's Appraisal"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__appraisal_by_manager
msgid "Managers"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__meeting_id
msgid "Meeting"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__message_has_error
msgid "Message Delivery error"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__message_ids
msgid "Messages"
msgstr "សារ"

#. module: hr_appraisal
#: selection:hr.employee,appraisal_frequency_unit:0
msgid "Month"
msgstr "ខែ"

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_search
msgid "My Activities"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_hr_appraisal_tree
msgid "Name"
msgstr "ឈ្មោះ"

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__activity_date_deadline
msgid "Next Activity Deadline"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__activity_summary
msgid "Next Activity Summary"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__activity_type_id
msgid "Next Activity Type"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__appraisal_date
msgid "Next Appraisal Date"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__message_needaction_counter
msgid "Number of Actions"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__count_completed_survey
msgid "Number of Answers"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__count_sent_survey
msgid "Number of Sent Forms"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__message_has_error_counter
msgid "Number of error"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,help:hr_appraisal.field_hr_appraisal__message_needaction_counter
msgid "Number of messages which requires an action"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,help:hr_appraisal.field_hr_appraisal__message_has_error_counter
msgid "Number of messages with delivery error"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,help:hr_appraisal.field_hr_appraisal__message_unread_counter
msgid "Number of unread messages"
msgstr ""

#. module: hr_appraisal
#: model:res.groups,name:hr_appraisal.group_hr_appraisal_user
msgid "Officer"
msgstr ""

#. module: hr_appraisal
#: selection:hr.appraisal,activity_state:0
msgid "Overdue"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__periodic_appraisal
msgid "Periodic Appraisal"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__periodic_appraisal_created
msgid "Periodic Appraisal has been created"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_employee_view_form
msgid "Periodicity"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_hr_appraisal_form
msgid "Plan"
msgstr ""

#. module: hr_appraisal
#: selection:hr.appraisal,activity_state:0
msgid "Planned"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__related_partner_id
msgid "Related Partner"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_employee_view_form
msgid "Repeat Every"
msgstr ""

#. module: hr_appraisal
#: model:ir.ui.menu,name:hr_appraisal.menu_hr_appraisal_report
msgid "Reporting"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__activity_user_id
msgid "Responsible User"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_kanban
msgid "Schedule The Final Interview"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_search
msgid "Search Appraisal"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_employee_view_form
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_hr_appraisal_form
msgid "Select Appraisal Reviewer..."
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__employee_survey_id
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__appraisal_self_survey_id
msgid "Self Appraisal"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_employee_view_form
msgid "Send Appraisal Form To"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__survey_sent_ids
msgid "Sent Forms"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_search
msgid "Show all records which has next action date is before today"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_hr_appraisal_form
msgid "Start Appraisal and Send Forms"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__state
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal_report__state
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_appraisal_report_search
msgid "Status"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,help:hr_appraisal.field_hr_appraisal__activity_state
msgid ""
"Status based on activities\n"
"Overdue: Due date is already passed\n"
"Today: Activity date is today\n"
"Planned: Future activities."
msgstr ""

#. module: hr_appraisal
#: model:ir.model,name:hr_appraisal.model_survey_user_input
msgid "Survey User Input"
msgstr ""

#. module: hr_appraisal
#: code:addons/hr_appraisal/models/hr_employee.py:76
#, python-format
msgid "The date of the next appraisal cannot be in the past"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,help:hr_appraisal.field_hr_employee__appraisal_date
msgid ""
"The date of the next appraisal is computed by the appraisal plan's dates "
"(first appraisal + periodicity)."
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,help:hr_appraisal.field_hr_appraisal__color
msgid "This color will be used in the kanban view."
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,help:hr_appraisal.field_hr_appraisal__collaborators_appraisal
msgid "This employee will be appraised by his collaborators"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,help:hr_appraisal.field_hr_appraisal__colleagues_appraisal
msgid "This employee will be appraised by his colleagues"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,help:hr_appraisal.field_hr_appraisal__manager_appraisal
msgid "This employee will be appraised by his managers"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,help:hr_appraisal.field_hr_appraisal__employee_appraisal
msgid "This employee will do a self-appraisal"
msgstr ""

#. module: hr_appraisal
#: code:addons/hr_appraisal/models/hr_appraisal.py:239
#: selection:hr.appraisal,state:0 selection:hr.appraisal.report,state:0
#: model_terms:ir.ui.view,arch_db:hr_appraisal.view_appraisal_report_search
#, python-format
msgid "To Start"
msgstr ""

#. module: hr_appraisal
#: selection:hr.appraisal,activity_state:0
msgid "Today"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_search
msgid "Today Activities"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__message_unread
msgid "Unread Messages"
msgstr "សារមិនទាន់អាន"

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__message_unread_counter
msgid "Unread Messages Counter"
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_appraisal__website_message_ids
msgid "Website Messages"
msgstr "សារវែបសាយ"

#. module: hr_appraisal
#: model:ir.model.fields,help:hr_appraisal.field_hr_appraisal__website_message_ids
msgid "Website communication history"
msgstr "ប្រវត្តិទំនាក់ទំនងវែបសាយ"

#. module: hr_appraisal
#: selection:hr.employee,appraisal_frequency_unit:0
msgid "Year"
msgstr ""

#. module: hr_appraisal
#: code:addons/hr_appraisal/models/hr_appraisal.py:232
#, python-format
msgid "You cannot delete appraisal which is in %s state"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.actions.act_window,help:hr_appraisal.open_view_hr_appraisal_tree
msgid ""
"You will be able to choose which forms will be sent (Employee,Managers,Collaborators or Colleagues),\n"
"            to whom and the evaluation deadline. Once you have defined it,\n"
"            change the stage to send it and view the results."
msgstr ""

#. module: hr_appraisal
#: model:ir.model.fields,field_description:hr_appraisal.field_hr_employee__appraisal_collaborators_survey_id
msgid "collaborate's Appraisal"
msgstr ""

#. module: hr_appraisal
#: model_terms:ir.ui.view,arch_db:hr_appraisal.hr_appraisal_kanban
msgid "oe_kanban_text_red"
msgstr ""
